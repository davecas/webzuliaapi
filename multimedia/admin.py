from django.contrib import admin

from .models import Image, Video, Chronicle

class ImageAdmin(admin.ModelAdmin):
    list_display = ('author','preview',)
    search_fields = ('author',)
    list_filter = ('author', 'clasification',)

class VideoAdmin(admin.ModelAdmin):
    list_display = ('author', 'preview',)
    search_fields = ('author',)
    list_filter = ('author', 'clasification', )

class ChronicleAdmin(admin.ModelAdmin):
    list_display = ('author','name', '_file',)
    search_fields = ('author', 'name', '_file',)
    list_filter = ('author', )

admin.site.register(Image, ImageAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Chronicle, ChronicleAdmin)
