#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.html import format_html

from django.db import models

from clasification.models import Author, Clasification

class Image(models.Model):
    image = models.FileField                        (upload_to='imagenes/%Y/%m/%d', verbose_name="Imagen")
    description = models.TextField                  (verbose_name="Descripción")
    clasification = models.ManyToManyField          (Clasification, verbose_name="Clasificación")
    author = models.ForeignKey                      (Author, verbose_name='Autor')

    class Meta:
        verbose_name        = 'Imagen'
        verbose_name_plural = 'Imagenes'

    def preview(self):
        return format_html("<a href='%s' target='_blank'><img src='%s' width='90' height='120' /></a>" % (self.image.url, self.image.url))

    def __str__(self):
        return u'%s - %s' % (self.author, self.description)

class Video(models.Model):
    url = models.TextField                   (verbose_name="Dirección url")
    description = models.TextField           ( verbose_name="Descripción")
    clasification = models.ManyToManyField   (Clasification, verbose_name="Clasificación")
    author = models.ForeignKey               (Author, verbose_name='Autor')

    class Meta:
        verbose_name        = 'Video'
        verbose_name_plural = 'Videos'

    def preview(self):
        return format_html("<iframe width='300' height='250' src='%s' frameborder='0' allowfullscreen></iframe>" % (self.url))

    def __unicode__(self):
        return u'%s - %s' % (self.author, self.url)

class Chronicle(models.Model):
    name = models.CharField      (max_length=150, verbose_name='Nombre')
    text = models.TextField      (verbose_name='Texto')
    _file = models.FileField     (upload_to='cronicas', verbose_name='Archivo')
    author = models.CharField    (max_length=150, verbose_name='Autor')

    class Meta:
        verbose_name        = 'Crónica'
        verbose_name_plural = 'Crónicas'

    def __unicode__(self):
        return u'%s - %s' % (self.name, self.author)
