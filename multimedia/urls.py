from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from .viewsets import ImageViewSet, VideoViewSet, ChronicleViewSet

simpleRouter = SimpleRouter()
simpleRouter.register(r'images', ImageViewSet)
simpleRouter.register(r'videos', VideoViewSet)
simpleRouter.register(r'chronicles', ChronicleViewSet)
routerUrls = simpleRouter.urls

urlpatterns = [
    url(r'^', include(routerUrls)),
]
