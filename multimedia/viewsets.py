from rest_framework import viewsets
from .models import Image, Video, Chronicle
from .serializers import ImageSerializer, VideoSerializer, ChronicleSerializer

class ImageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Image.objects.all()
    serializer_class = ImageSerializer

class VideoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Video.objects.all()
    serializer_class = VideoSerializer

class ChronicleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Chronicle.objects.all()
    serializer_class = ChronicleSerializer
