from rest_framework import serializers
from .models import Image, Video, Chronicle

from clasification.serializers import AuthorSerializer, ClasificationSerializer

class ImageSerializer(serializers.ModelSerializer):

    clasification = ClasificationSerializer (many=True, read_only=True)
    author = AuthorSerializer(many=False, read_only= True)

    class Meta:
        model = Image
        fields = ('id', 'image', 'description', 'clasification', 'author',)

class VideoSerializer(serializers.ModelSerializer):

    clasification = ClasificationSerializer (many=True, read_only=True)
    author = AuthorSerializer(many=False, read_only= True)

    class Meta:
        model = Video
        fields = ('id', 'url', 'description', 'clasification', 'author',)

class ChronicleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Chronicle
        fields = ('id', 'name', 'text', '_file', 'author',)
