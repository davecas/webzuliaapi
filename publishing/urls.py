from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from .viewsets import ReportViewSet, SpecialViewSet, PublicationViewSet

simpleRouter = SimpleRouter()
simpleRouter.register(r'reports', ReportViewSet)
simpleRouter.register(r'specials', SpecialViewSet)
simpleRouter.register(r'publications', PublicationViewSet)
routerUrls = simpleRouter.urls

urlpatterns = [
    url(r'^', include(routerUrls)),
]
