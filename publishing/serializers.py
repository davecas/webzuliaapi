from rest_framework import serializers

from multimedia.serializers import ImageSerializer, VideoSerializer
from clasification.serializers import ClasificationSerializer

from profile_user.serializers import UserSerializer

from .models import Report, Special, Publication

class ReportSerializer(serializers.ModelSerializer):
    images = ImageSerializer (many=True, read_only=True)
    videos = VideoSerializer (many=True, read_only=True)
    user = UserSerializer (many=False, read_only=True)

    class Meta:
        model = Report
        fields = ('id', 'pre_title', 'title', 'text', 'images', 'videos', 'user', 'date',)

class  SpecialSerializer(serializers.ModelSerializer):

    images = ImageSerializer (many=True, read_only=True)
    videos = VideoSerializer (many=True, read_only=True)

    class Meta:
        model =  Special
        fields = ('id', 'title', 'summary', 'text', 'images', 'videos', 'author', 'date',)

class PublicationSerializer(serializers.ModelSerializer):

    images = ImageSerializer (many=True, read_only=True)
    videos = VideoSerializer (many=True, read_only=True)
    clasification = ClasificationSerializer (many=False, read_only=True)

    class Meta:
        model = Publication
        fields = ('id', 'name', 'text', '_file', 'images', 'videos', 'clasification', 'author',)
