from rest_framework import viewsets
from rest_framework import filters

from .models      import Report, Special, Publication
from .serializers import ReportSerializer, SpecialSerializer, PublicationSerializer

class ReportViewSet(viewsets.ReadOnlyModelViewSet):
    queryset            = Report.objects.all()
    serializer_class    = ReportSerializer
    filter_backends     = (filters.DjangoFilterBackend, filters.SearchFilter,)
    filter_fields       = ('id', 'pre_title', 'title',)
    search_fields       = ('pre_title', 'title',)

class SpecialViewSet(viewsets.ReadOnlyModelViewSet):
    queryset            = Special.objects.all()
    serializer_class    = SpecialSerializer
    filter_backends     = (filters.DjangoFilterBackend, filters.SearchFilter,)
    filter_fields       = ('id', 'title', 'summary', )
    search_fields       = ('title', 'summary')

class PublicationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset            = Publication.objects.all()
    serializer_class    = PublicationSerializer
    filter_backends     = (filters.DjangoFilterBackend, filters.SearchFilter,)
    filter_fields       = ('id', 'name',)
    search_fields       = ('name')
