from django.contrib import admin

from .models import Special, Report, Publication

class SpecialAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'author', )
    search_fields = ('title', 'summary', 'date')
    list_filter = ('date', 'author')
    filter_horizontal = ('images', 'videos',)

class ReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'user', )
    search_fields = ('title', 'pre_title', 'date')
    list_filter = ('date', 'user__username')
    filter_horizontal = ('images', 'videos',)

class PublicationAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', )
    search_fields = ('name', 'author', 'text')
    list_filter = ('clasification__name',)
    filter_horizontal = ('images', 'videos',)

admin.site.register(Special, SpecialAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(Publication, PublicationAdmin)
