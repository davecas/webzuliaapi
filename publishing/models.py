#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from multimedia.models import Image,Video
from clasification.models import Clasification

class Special(models.Model):
    title = models.CharField          (max_length=150, verbose_name='Titulo')
    summary = models.CharField        (max_length=150, verbose_name='Sumario')
    text = models.TextField           (verbose_name='Texto')
    images = models.ManyToManyField   (Image, verbose_name='Imagenes')
    videos = models.ManyToManyField   (Video, verbose_name="Videos", blank=True)
    author = models.CharField         (max_length=150,  verbose_name='Autor')
    date = models.DateTimeField       (verbose_name='Fecha')

    class Meta:
        verbose_name        = 'Especial'
        verbose_name_plural = 'Especiales'

    def __str__(self):
        return u'%s - %s - %s' % (self.title, self.date , self.author)

class Report(models.Model):
    pre_title = models.CharField      (max_length=100, verbose_name='Pre-Titulo')
    title = models.CharField          (max_length=150, verbose_name='Titulo')
    text = models.TextField           (verbose_name='Noticia')
    images = models.ManyToManyField   (Image, verbose_name='Imagenes')
    videos = models.ManyToManyField   (Video, verbose_name="Videos", blank=True)
    user = models.ForeignKey          (User, verbose_name='Escrito por')
    date = models.DateTimeField       (verbose_name='Fecha')

    def __str__(self):
        return u'%s - %s - %s' % (self.title, self.date , self.user)

class Publication(models.Model):
    name = models.CharField            (max_length=100, verbose_name="Nombre")
    text = models.TextField            (verbose_name="Texto")
    _file = models.FileField           (upload_to='Publicaciones/%Y/%m/%d', verbose_name="Archivo")
    images = models.ManyToManyField    (Image, verbose_name='Imagenes', blank=True)
    videos = models.ManyToManyField    (Video, verbose_name="Videos", blank=True)
    clasification = models.ForeignKey  (Clasification, verbose_name="Clasificación")
    author = models.CharField          (max_length=100, verbose_name="Autor")

    class Meta:
        verbose_name        = 'Publicacion'
        verbose_name_plural = 'Publicaciones'

    def __str__(self):
        return u'%s - %s' % (self.name, self.author)
