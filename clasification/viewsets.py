from rest_framework import viewsets
from .models import Author, Clasification, Map
from .serializers import AuthorSerializer, ClasificationSerializer, MapSerializer

class AuthorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Author.objects.all()
    serializer_class = AuthorSerializer

class ClasificationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Clasification.objects.all()
    serializer_class = ClasificationSerializer

class MapViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Map.objects.all()
    serializer_class = MapSerializer
