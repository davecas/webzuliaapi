from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from .viewsets import AuthorViewSet, ClasificationViewSet, MapViewSet

simpleRouter = SimpleRouter()
simpleRouter.register(r'authors', AuthorViewSet)
simpleRouter.register(r'clasifications', ClasificationViewSet)
simpleRouter.register(r'maps', MapViewSet)
routerUrls = simpleRouter.urls

urlpatterns = [
    url(r'^', include(routerUrls)),
]
