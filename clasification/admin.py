from django.contrib import admin

from .models import Author, Clasification, Map

class MapAdmin(admin.ModelAdmin):
    list_display = ('name','description','preview',)
    search_fields = ('name',)

admin.site.register(Author)
admin.site.register(Clasification)
admin.site.register(Map, MapAdmin)
