from rest_framework import serializers
from .models import Author, Clasification, Map

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author

class ClasificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clasification

class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Map
        fields = ('id', 'name', 'description', 'image', )
