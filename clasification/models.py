#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.html import format_html

from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nombre')

    class Meta:
        verbose_name        = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return u'%s' % self.name

class Clasification(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")

    class Meta:
        verbose_name        = 'Clasificacion'
        verbose_name_plural = 'Clasificaciones'

    def __str__(self):
        return u'%s' % self.name

class Map(models.Model):
    SHIRT_SIZES = (
        ('R', 'Regional'),
        ('M', 'Municipal'),
    )
    name = models.CharField           (max_length=150, verbose_name='Nombre')
    description = models.TextField    (verbose_name='Descripción')
    image = models.FileField          (upload_to='maps/', verbose_name='Imagen')

    class Meta:
        verbose_name        = 'Mapa'
        verbose_name_plural = 'Mapas'

    def preview(self):
        return format_html("<a href='%s' target='_blank'><img src='%s' width='90' height='120' /></a>" % (self.image.url, self.image.url))

    def __str__(self):
        return u'%s - %s' % (self.name, self.description)
