from rest_framework import serializers

from multimedia.serializers import ChronicleSerializer

from .models import People, Organization, Chronicler

class PeopleSerializer(serializers.ModelSerializer):

    class Meta:
        model = People
        fields = ('id', 'name', 'description', 'birth', 'image', 'place_of_birth', 'area',)

class ChroniclerSerializer(serializers.ModelSerializer):

    chronicle = ChronicleSerializer (many=True, read_only=True)

    class Meta:
        model = Chronicler
        fields = ('id', 'name', 'description', 'age', 'avatar', 'chronicle',)

class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name', 'position', 'period', 'avatar', 'location', 'contact', )
