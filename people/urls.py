from rest_framework.routers import SimpleRouter
from django.conf.urls import url, include
from .viewsets import PeopleViewSet, ChroniclerViewSet, OrganizationViewSet

simpleRouter = SimpleRouter()
simpleRouter.register(r'peoples', PeopleViewSet)
simpleRouter.register(r'chroniclers', ChroniclerViewSet)
simpleRouter.register(r'organizations', OrganizationViewSet)
routerUrls = simpleRouter.urls

urlpatterns = [
    url(r'^', include(routerUrls)),
]
