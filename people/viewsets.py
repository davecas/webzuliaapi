from rest_framework import viewsets
from .models import People, Organization, Chronicler
from .serializers import PeopleSerializer, ChroniclerSerializer, OrganizationSerializer

class PeopleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = People.objects.all()
    serializer_class = PeopleSerializer

class ChroniclerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Chronicler.objects.all()
    serializer_class = ChroniclerSerializer

class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset         = Organization.objects.all()
    serializer_class = OrganizationSerializer
