#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.html import format_html

from django.db import models

from multimedia.models import Chronicle

class Chronicler(models.Model):
    name = models.CharField             (max_length=100, verbose_name='Nombre')
    description = models.TextField      (verbose_name='Descripción')
    age = models.CharField              (max_length=10, verbose_name='Edad')
    avatar = models.FileField           (upload_to='imagenes/cronistas/')
    chronicle = models.ManyToManyField  (Chronicle, verbose_name='Cronicas', blank=True)

    class Meta:
        verbose_name        = 'Cronista'
        verbose_name_plural = 'Cronistas'

    def preview(self):
        return format_html("<a href='%s' target='_blank'><img src='%s' width='90' height='120' /></a>" % (self.avatar.url, self.avatar.url))

    def __str__(self):
        return u'%s - %s' % (self.name, self.description)

class People(models.Model):
    name = models.CharField                (max_length=150, verbose_name='Nombre')
    description = models.TextField         (verbose_name='Descripción')
    birth = models.DateTimeField           (verbose_name='Fecha de nacimiento')
    image = models.FileField               (upload_to='gente/')
    place_of_birth = models.CharField      (max_length=150, verbose_name='Lugar de nacimiento')
    area = models.CharField                (max_length=150,  verbose_name='Nivel Edcuativo')

    class Meta:
        verbose_name        = 'Gente'
        verbose_name_plural = 'Gentes'

    def preview(self):
        return format_html("<a href='%s' target='_blank'><img src='%s' width='90' height='120' /></a>" % (self.imagen.url, self.imagen.url))

    def __str__(self):
        return u'%s - %s' % (self.name, self.description)

class Organization(models.Model):
    name = models.CharField              (max_length=100, verbose_name='Nombre')
    position = models.CharField          (max_length=100, verbose_name='Cargo')
    period = models.CharField            (max_length=100, verbose_name='Periodo')
    avatar = models.FileField            (upload_to='imagenes/organizacion/')
    location = models.CharField          (max_length=150, verbose_name='Lugar de acción')
    contact = models.CharField           (max_length=100, verbose_name='Lugar a contactar')

    class Meta:
        verbose_name        = 'Organizacion'
        verbose_name_plural = 'Organizaciones'

    def preview(self):
        return format_html("<a href='%s' target='_blank'><img src='%s' width='90' height='120' /></a>" % (self.avatar.url, self.avatar.url))

    def __str__(self):
        return u'%s - %s' % (self.name, self.position)
