# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-19 17:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('multimedia', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chronicler',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nombre del cronista')),
                ('description', models.TextField(verbose_name='Descripci\xf3n del cronista')),
                ('age', models.CharField(max_length=10)),
                ('avatar', models.FileField(upload_to='imagenes/cronistas/')),
                ('chronicle', models.ManyToManyField(blank=True, to='multimedia.Chronicle', verbose_name='Cronicas')),
            ],
            options={
                'verbose_name': 'Cronista',
                'verbose_name_plural': 'Cronistas',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nombre del adjudicado')),
                ('position', models.CharField(max_length=100, verbose_name='Cargo adjudicado')),
                ('period', models.CharField(max_length=100, verbose_name='Periodo del adjudicado')),
                ('avatar', models.FileField(upload_to='imagenes/organizacion/')),
                ('location', models.CharField(max_length=150, verbose_name='Lugar de acci\xf3n')),
                ('contact', models.CharField(max_length=100, verbose_name='Lugar a contactar')),
            ],
            options={
                'verbose_name': 'Organizacion',
                'verbose_name_plural': 'Organizaciones',
            },
        ),
        migrations.CreateModel(
            name='People',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre de la persona')),
                ('description', models.TextField(verbose_name='Descripcion de la persona')),
                ('birth', models.DateTimeField(verbose_name='Fecha de nacimiento')),
                ('image', models.FileField(upload_to='gente/', verbose_name='Avatar de la persona')),
                ('place_of_birth', models.CharField(max_length=150, verbose_name='Lugar de nacimiento')),
                ('area', models.CharField(max_length=150, verbose_name='Nivel Edcuativo')),
            ],
            options={
                'verbose_name': 'Gente',
                'verbose_name_plural': 'Gentes',
            },
        ),
    ]
