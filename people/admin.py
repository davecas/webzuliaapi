from django.contrib import admin

from .models import Chronicler, People, Organization

class ChroniclerAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'preview',)
    search_fields = ('name', 'description',)
    filter_horizontal = ('chronicle',)

class PeopleAdmin(admin.ModelAdmin):
    list_display = ('name','description','preview',)
    search_fields = ('name',)


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'position', 'preview', )
    search_fields = ('name',)

admin.site.register(Chronicler, ChroniclerAdmin)
admin.site.register(People, PeopleAdmin)
admin.site.register(Organization, OrganizationAdmin)
